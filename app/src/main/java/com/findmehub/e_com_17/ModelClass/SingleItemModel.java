package com.findmehub.e_com_17.ModelClass;

/**
 * Created by Abhishek on 27-09-2017.
 */

public class SingleItemModel {
    private String name;
    private String url;
    private String description;
    int images;


    public SingleItemModel() {
    }

    public SingleItemModel(String name, String url,int images) {
        this.name = name;
        this.url = url;
        this.images = images;
    }

    public int getImages() {
        return images;
    }

    public void setImages(int images) {
        this.images = images;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
