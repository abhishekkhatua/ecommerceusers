package com.findmehub.e_com_17.ModelClass;

/**
 * Created by Abhishek on 22-09-2017.
 */

public class WishRetriver {
    int images;


    public WishRetriver(int images) {
        this.images = images;
    }

    public int getImages() {
        return images;
    }

    public void setImages(int images) {
        this.images = images;
    }
}
