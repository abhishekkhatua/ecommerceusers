package com.findmehub.e_com_17;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.findmehub.e_com_17.Adapter.ImageAdapter;
import com.findmehub.e_com_17.Adapter.MainContent;
import com.findmehub.e_com_17.ModelClass.Data;
import com.findmehub.e_com_17.ModelClass.SectionDataModel;
import com.findmehub.e_com_17.ModelClass.SingleItemModel;

import java.util.ArrayList;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */
public class HomeFragment extends Fragment {
    ViewPager pager;
    PagerAdapter adapter;
    int [] flag;
    private RecyclerView recycler_view_two,recycler_view2;
    private List<Data> data;
    private MainContent verticalAdapter ;
    ArrayList<SectionDataModel> allSampleData = new ArrayList<>();

    static Integer[] drawableArray = {R.drawable.shoes, R.drawable.shoes, R.drawable.shirts,
            R.drawable.shoes,R.drawable.shoes,R.drawable.shirts};

    public HomeFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_blank, container, false);
        pager     = (ViewPager)view.findViewById(R.id.pager);
        createDummyData();

        recycler_view2 = (RecyclerView)view.findViewById(R.id.recycler_view2);
        flag = new int[] { R.drawable.shop_pic, R.drawable.royal_challnge };
        adapter = new ImageAdapter(getContext(),flag);
        int margin = (int)TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 20*2,     getResources().getDisplayMetrics());
        pager.setPageMargin(-margin);
        pager.setAdapter(adapter);
        List<Data> data = new ArrayList<>();
        data.add(new Data(R.drawable.ic_favorite_black_24dp, "WishList"));
        data.add(new Data( R.drawable.ic_store_black_24dp, "Shop"));
        data.add(new Data( R.drawable.ic_local_offer_black_24dp, "Offers"));
        data.add(new Data( R.drawable.ic_shopping_cart_black_24dp, "Cart"));
        data.add(new Data( R.drawable.ic_home_black_24dp, "Categories"));

        recycler_view2.setHasFixedSize(true);

        MainContent adapter = new MainContent(getContext(), allSampleData);

        recycler_view2.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));

        recycler_view2.setAdapter(adapter);
        return view;
    }

    public void createDummyData() {
        for (int i = 1; i <= 5; i++) {

            SectionDataModel dm = new SectionDataModel();

            dm.setHeaderTitle("Section " + i);

            ArrayList<SingleItemModel> singleItem = new ArrayList<SingleItemModel>();
            for (int j = 0; j <= 5; j++) {
                singleItem.add(new SingleItemModel("Item " + j, "URL " + j,drawableArray[j]));
            }

            dm.setAllItemsInSection(singleItem);

            allSampleData.add(dm);

        }
    }
}
