package com.findmehub.e_com_17;

import android.app.Fragment;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.findmehub.e_com_17.Fragments.ProductFragment;
import com.findmehub.e_com_17.Fragments.WishFragment;

public class MainActivity extends AppCompatActivity {
    private DrawerLayout drawerLayout;
    private Toolbar toolbar;

    NavigationView navigationView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
         navigationView = (NavigationView)findViewById(R.id.navigation_view);

        initNavigationDrawer();

    }
    public void initNavigationDrawer() {
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(MenuItem menuItem) {

                int id = menuItem.getItemId();

                switch (id){
                    case R.id.home:
                        FragmentManager fragmentManager = getSupportFragmentManager();
                        FragmentTransaction ft = fragmentManager.beginTransaction();
                        HomeFragment homeFragment = new HomeFragment();
                        ft.replace(R.id.container, homeFragment);
                        ft.commit();
                        drawerLayout.closeDrawers();
                        break;
                    case R.id.settings:
                        FragmentManager fragmentManager1 = getSupportFragmentManager();
                        FragmentTransaction ft1 = fragmentManager1.beginTransaction();
                        WishFragment wishFragment = new WishFragment();
                        ft1.replace(R.id.container, wishFragment);
                        ft1.commit();
                        drawerLayout.closeDrawers();
                        break;
                    case R.id.shops:
                        FragmentManager fragmentManager2 = getSupportFragmentManager();
                        FragmentTransaction ft2 = fragmentManager2.beginTransaction();
                        ProductFragment productFragment = new ProductFragment();
                        ft2.replace(R.id.container,productFragment);
                        ft2.commit();
                        drawerLayout.closeDrawers();
                        break;
//                    case R.id.logout:
//                        finish();

                }
                return true;
            }
        });
        View header = navigationView.getHeaderView(0);
        TextView tv_email = (TextView)header.findViewById(R.id.tv_email);
        tv_email.setText("abhishek.com");
        drawerLayout = (DrawerLayout)findViewById(R.id.drawer);

        ActionBarDrawerToggle actionBarDrawerToggle = new ActionBarDrawerToggle(this,drawerLayout,toolbar,R.string.drawer_open,R.string.drawer_close){

            @Override
            public void onDrawerClosed(View v){
                super.onDrawerClosed(v);
            }

            @Override
            public void onDrawerOpened(View v) {
                super.onDrawerOpened(v);
            }
        };
        drawerLayout.addDrawerListener(actionBarDrawerToggle);
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();

        HomeFragment homeFragment = new HomeFragment();
        fragmentTransaction.replace(R.id.container, homeFragment,"fragments");
        fragmentTransaction.commit();
        drawerLayout.closeDrawers();
        actionBarDrawerToggle.syncState();
    }
}
