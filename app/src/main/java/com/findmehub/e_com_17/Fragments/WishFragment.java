package com.findmehub.e_com_17.Fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.findmehub.e_com_17.Adapter.WishListAdapter;
import com.findmehub.e_com_17.R;

import java.util.ArrayList;
import java.util.Arrays;


/**
 * A simple {@link Fragment} subclass.
 */
public class WishFragment extends Fragment {
    ArrayList personImages = new ArrayList<>(Arrays.asList( R.drawable.shop_pic, R.drawable.shop_pic, R.drawable.shop_pic,R.drawable.shop_pic));
    private LinearLayoutManager mLinearLayoutManager;

    public WishFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragments_wishlist, container, false);
        RecyclerView recyclerView = (RecyclerView)view.findViewById(R.id.recycler_wishlist);

        mLinearLayoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(mLinearLayoutManager);
        //  call the constructor of CustomAdapter to send the reference and data to Adapter
        WishListAdapter wishListAdapter = new WishListAdapter(getContext(),personImages);
        recyclerView.setAdapter(wishListAdapter);
        return view;
    }

}
