package com.findmehub.e_com_17.Adapter;

import android.content.Context;
import android.graphics.Paint;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.findmehub.e_com_17.ModelClass.WishRetriver;
import com.findmehub.e_com_17.R;

import java.util.ArrayList;



/**
 * Created by sbuser on 14-Jul-17.
 */

public class WishListAdapter extends RecyclerView.Adapter<WishListAdapter.MyViewHolder> {
    ArrayList personImages;
    ArrayList<WishRetriver>wishRetrivers = new ArrayList<>();
    Context context;

    public WishListAdapter(Context context, ArrayList personImages) {
        this.context = context;
        this.personImages = personImages;
    }
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // infalte the item Layout
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.wishlist_layout, parent, false);
        // set the view's size, margins, paddings and layout parameters
        MyViewHolder vh = new MyViewHolder(v); // pass the view to View Holder
        return vh;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        holder.image.setImageResource((Integer) personImages.get(position));


    }

    @Override
    public int getItemCount() {
        return personImages.size();
    }
    public class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView image;
        TextView wish_title,text__rs,discount_wish,add_bags_wish;
        public MyViewHolder(View itemView) {
            super(itemView);
            image = (ImageView) itemView.findViewById(R.id.image_wish);
            wish_title =(TextView)itemView.findViewById(R.id.wish_title);
            text__rs   = (TextView)itemView.findViewById(R.id.text__rs);
            discount_wish =(TextView)itemView.findViewById(R.id.discount_wish);
            discount_wish.setPaintFlags(discount_wish.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);

//            if (discount_wish.getText().toString().trim().equals("false")){
//                Log.i("I WAS HERE",discount_wish.getText().toString().trim());
//                discount_wish.setVisibility(View.INVISIBLE);
//
//            }
//            else {
//                discount_wish.setVisibility(View.VISIBLE);
//
//            }
        }
    }
}
