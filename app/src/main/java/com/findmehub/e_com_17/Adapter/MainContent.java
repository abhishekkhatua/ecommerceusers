package com.findmehub.e_com_17.Adapter;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;


import com.findmehub.e_com_17.MainActivity;
import com.findmehub.e_com_17.ModelClass.Data;
import com.findmehub.e_com_17.ModelClass.SectionDataModel;
import com.findmehub.e_com_17.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;



/**
 * Created by Abhishek on 29-05-2017.
 */

public class MainContent extends RecyclerView.Adapter<MainContent.ItemRowHolder> {

private ArrayList<SectionDataModel> dataList;
private Context mContext;

public MainContent(Context context, ArrayList<SectionDataModel> dataList) {
        this.dataList = dataList;
        this.mContext = context;
        }

@Override
public ItemRowHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.list_item, null);
        ItemRowHolder mh = new ItemRowHolder(v);
        return mh;
        }

@Override
public void onBindViewHolder(ItemRowHolder itemRowHolder, int i) {

final String sectionName = dataList.get(i).getHeaderTitle();

        ArrayList singleSectionItems = dataList.get(i).getAllItemsInSection();

        itemRowHolder.itemTitle.setText(sectionName);

        SectionListDataAdapter itemListDataAdapter = new SectionListDataAdapter(mContext, singleSectionItems);

        itemRowHolder.recycler_view_list.setHasFixedSize(true);
        itemRowHolder.recycler_view_list.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false));
        itemRowHolder.recycler_view_list.setAdapter(itemListDataAdapter);


//        itemRowHolder.btnMore.setOnClickListener(new View.OnClickListener() {
//@Override
//public void onClick(View v) {
//
//
//        Toast.makeText(v.getContext(), "click event on more, "+sectionName , Toast.LENGTH_SHORT).show();
//
//
//
//        }
//        });


       /* Glide.with(mContext)
                .load(feedItem.getImageURL())
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .centerCrop()
                .error(R.drawable.bg)
                .into(feedListRowHolder.thumbView);*/
        }

@Override
public int getItemCount() {
        return (null != dataList ? dataList.size() : 0);
        }

public class ItemRowHolder extends RecyclerView.ViewHolder {

    protected TextView itemTitle;

    protected RecyclerView recycler_view_list;
    protected Button btnMore;


    public ItemRowHolder(View view) {
        super(view);

        this.itemTitle = (TextView) view.findViewById(R.id.itemTitle);
        this.recycler_view_list = (RecyclerView) view.findViewById(R.id.recycler_view_list);
        this.btnMore = (Button) view.findViewById(R.id.btnMore);


    }
}
}