package com.findmehub.e_com_17.Adapter;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.findmehub.e_com_17.R;

import java.util.List;

/**
 * Created by Abhishek on 19-09-2017.
 */

public class ImageAdapter extends PagerAdapter {

    Context context;
    int[] flag;
    LayoutInflater layoutInflater;


    public ImageAdapter(Context context, int[] flag) {
        this.context = context;
        this.flag = flag;
    }


    @Override
    public int getCount() {
        return flag.length;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((LinearLayout) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        ImageView images_view;
        layoutInflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(R.layout.images_pagerviewer, container, false);
        images_view = (ImageView) view.findViewById(R.id.images_view);
//            images_view.setImageResource(images[position]);
        images_view.setImageResource(flag[position]);

        ((ViewPager) container).addView(view);
        return view;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        ((ViewPager) container).removeView((LinearLayout) object);
    }
}
