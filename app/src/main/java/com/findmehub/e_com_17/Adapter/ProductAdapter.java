package com.findmehub.e_com_17.Adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.findmehub.e_com_17.R;

/**
 * Created by Abhishek on 05-10-2017.
 */

public class ProductAdapter extends RecyclerView.Adapter<ProductAdapter.ItemViewHolder> {

    @Override
    public ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item, null);
        return null;
    }

    @Override
    public void onBindViewHolder(ItemViewHolder holder, int position) {

    }

    @Override
    public int getItemCount() {
        return 0;
    }
    public class ItemViewHolder extends RecyclerView.ViewHolder{


        public ItemViewHolder(View itemView) {
            super(itemView);
        }
    }
}
